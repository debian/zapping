#ifndef __MIXER_H__
#define __MIXER_H__

/* Startup/shutdown, as usual */
void		startup_mixer(tveng_device_info *info);
void		shutdown_mixer(tveng_device_info *info);

#endif
